#include <stdio.h>
int main(){
	char l;
	printf("Enter a letter:");
	scanf("%s", &l);
	
	switch (l){
		case 'a':
		case 'e':
		case 'i':
		case 'o':
		case 'u':
		case 'A':
		case 'E':
		case 'I':
		case 'O':
		case 'U':
			printf("The letter is a Vowel\n");
			break;
		default:
			printf("The letter is a Consonant\n");
	}
	return 0;
}
