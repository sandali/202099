#include <stdio.h>


struct student{

    char name[10];
    char subject[10];
    float marks;
};

int main(){
    int i,n;
    
    printf("Enter the number of stdents:");
    scanf("%d",&n);
    
    printf("\n");
    
    if (n<5){
        
         printf("Sorry.This is made for a minimun of 5 students data to be entered at a time.");
        
    }
    
    else{
        struct student person[n];
        for (i=0;i<n;i++){
            
            printf("Enter details of student No.%d\n",i+1);
            
            printf("Enter the name of the student:");
            scanf("%s",person[i].name);
                
            printf("Enter the subject name:");
            scanf("%s",person[i].subject);
                
            printf("Enter the mark:");
            scanf("%f",&person[i].marks);
            
            printf("\n");
            
        }
        
        printf("\n");
        
        for (i=0;i<n;i++){
            
            printf("Details of student No.%d\n",i+1);
            
            printf("The Name is: %s\n",person[i].name);
            printf("The Subject: %s\n",person[i].subject);
            printf("Marks obtained: %.2f\n",person[i].marks);
            
            printf("\n");
            
        }
        
    }
    
    return 0;
}
