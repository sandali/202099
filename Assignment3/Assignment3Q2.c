#include <stdio.h>
int main(){
	int num,i,check=0;
	printf("Enter a positive number:");
	scanf("%d", &num);
	if (num==1){
		printf("1 is neither a primary nor composite number\n");
	}
	else if (num<=0){
		printf("The number you entered isn't a positive number. Please try again.\n");
	}
	else{
		for (i=1; i<=num; i++){
			if (num%i==0){
				check++;
			}
		}
		if (check>2){
			printf("%d is not a prime number\n",num);
		}
		else {
			printf("%d is a prime number\n",num);
		}
	}
	return 0;
}

