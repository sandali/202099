#include<stdio.h>

#define ROWS 2
#define COLS 3

void readm1(int[ROWS][COLS]);
void readm2(int[COLS][ROWS]);
void print(int[ROWS][ROWS]);

int main(){
	int m1[ROWS][COLS];
	int m2[COLS][ROWS];
	int m3[ROWS][ROWS]={0,0,0,0};
	int i,j,k;

	readm1(m1);
	readm2(m2);

	for (i=0;i<ROWS;i++){
		for (j=0;j<ROWS;j++){
			for (k=0;k<COLS;k++){
				m3[i][j] += m1[i][k]*m2[k][j];
			}
		}
	}
	print(m3);

	return 0;
}

void readm1(int m1[ROWS][COLS]){
	printf("Enter 2 X 3 matrix\n");
	for(int i=0; i<ROWS; i++)
		for(int j=0; j<COLS; j++)
			scanf("%d",&m1[i][j]);
}

void readm2(int m2[COLS][ROWS]){
	printf("Enter 3 X 2 matrix\n");
	for(int i=0; i<COLS; i++)
		for(int j=0; j<ROWS; j++)
			scanf("%d",&m2[i][j]);
}

void print(int m3[ROWS][ROWS]){
	printf("Answer\n");
	for (int i=0;i<ROWS;i++){
		for (int j=0; j<ROWS;j++)
			printf("%d ",m3[i][j]);
		printf("\n");
	}
}

