#include<stdio.h>

void pattern1(int);
void pattern2(int);

void pattern1(int a){
	if (a>=1){
		printf("%d",a);
		pattern1(a-1);
	}
}

void pattern2(int n){
	
	if (n>=1){
		pattern2(n-1);
	}
	pattern1(n);
	printf("\n");

}
